## ZPLUG - ZSH Plugin manager
## NOTE: Check if zplug is installed
if [[ ! -d ~/.zplug ]]; then
  git clone https://github.com/zplug/zplug ~/.zplug
  source ~/.zplug/init.zsh && zplug update --self
fi
source ~/.zplug/init.zsh

zplug 'zplug/zplug', hook-build:'zplug --self-manage'

zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting"
zplug "nachoparker/tab_list_files_zsh_widget", use:"*.sh" # https://ownyourbits.com/2017/01/30/list-files-without-stopping-to-type-in-zsh/
zplug "plugins/colored-man-pages",   from:oh-my-zsh
zplug "romkatv/powerlevel10k", as:theme, depth:1

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load


# Disabling suggestion for large buffers, avoid triggering autosuggestion for strings that are too long
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

HISTFILE=~/.zsh_history

## TODO: what's this?
# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

## Set editor to NeoVim
export EDITOR='/usr/local/bin/nvim'
export VISUAL=$EDITOR
export SUDO_EDITOR=$EDITOR


## Replace behavior of Y to copy whole lite to system's clipboard
# Inspired from: https://stackoverflow.com/a/62141665/11106413
function vi-yank-line-xclip {
	zle vi-yank
	echo "$CUTBUFFER" | xclip -i -selection clipboard
}
zle -N vi-yank-line-xclip
bindkey -M vicmd "Y" vi-yank-line-xclip

## CTRL+arrow to move around one word at a time
# https://unix.stackexchange.com/a/140499/350611
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

## CTRL-backspace to delete word before cursor
## https://unix.stackexchange.com/a/117162/350611
bindkey "^\b" backward-kill-word

bindkey -v "^N" history-incremental-search-backward # backward search in history for a string, it doesn't care about what's already in there (press ^R to go next)
bindkey -v "^P" history-incremental-search-forward # backward search in history for a string, it doesn't care about what's already in there (press ^R to go next)

bindkey "^XP" push-line-or-edit # execute a command before the current one # TODO doesn't work in vicmd mode because 'put' takes precedence when pressing 'p'

## beginning-of-line & end-of-line
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line

bindkey -v "^XS" "transpose-words" # Transpose the two preceding words

bindkey -v "^XD" "copy-prev-shell-word" # Duplicate WORD of the left at cursor position

bindkey "^K" kill-line
bindkey "^U" backward-kill-line

bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward

HISTSIZE=1000
SAVEHIST=1000

setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt auto_cd # cd into directories without cd prefix
setopt cdable_vars # Allow CD into custom directory alias, see below cloc_s...
setopt inc_append_history # all sessions write to HISTFILE
setopt extended_history

## Directory shortcuts
## use cd <saved_shortcut>
## cloc_save to save a new shortcut pointing the current directory (PWD)
alias cloc_show='cat ~/.saved_dirs'

function cloc_save() {
  	here=$(pwd)
  	if [ $# -eq 0 ]
  	then
    	name=$(basename $here)
  	elif [ $# -eq 1 ]
  	then
    	name=$1
  	elif [ $# -eq 2 ]
  	then
    	name=$(basename $2)
  	elif [ $# > 2 ]
  	then
    	echo "usage: save [<name>] OR cloc_save [<name>] [<location>]"
    	return -1
  	fi

  	sed -i -e "/^$name=/d" ~/.saved_dirs
  	echo "$name=\"$here\"" >> ~/.saved_dirs
  	source ~/.saved_dirs
}

source ~/.saved_dirs

export PATH=~/.local/bin:$PATH

## Open NeoVim with `e`
e() {
  	nvim $1
}

## https://superuser.com/a/1397388/1029143
# Special ranger function to gradually move directory 
function ranger-cd() {
  	# create a temp file and store the name
  	tempfile="$(mktemp -t tmp.XXXXXX)"

    # run ranger and ask it to output the last path into the
    # temp file
    ranger --choosedir="$tempfile" "${@:-$(pwd)}"

    # if the temp file exists read and the content of the temp
    # file was not equal to the current path
    test -f "$tempfile" &&
       if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        	 # change directory to the path in the temp file
        	 cd -- "$(cat "$tempfile")"
       fi

    # its not super necessary to have this line for deleting
    # the temp file since Linux should handle it on the next
    # boot
    rm -f -- "$tempfile"
 }
## Use above function with the shortcut `nav`
alias nav="ranger-cd"

## Long listing alias
alias ll="ls -l --color --all --human-readable"

# RM interactive prompt once before removing more than three files, or recursive
alias rm="rm -I"

## https://unix.stackexchange.com/a/327572/350611
## vi mode config
## ---------------
## Activate vi mode.
bindkey -v
## Reduce vi-mode switching delay.
export KEYTIMEOUT=5

## Open TMUX on terminal session startup
if [ "$TMUX" = "" ]; then tmux; fi

## NOTE: MAY STAY AT THE VERY BOTTOM OF THE FILE (unsure about this)
## Load powerlevel10k configuration
## To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#export APACHE_RUN_DIR=/var/www
# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/home/nicolas/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
