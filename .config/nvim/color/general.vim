highlight Normal guibg=NONE ctermbg=NONE

highlight Type guifg=#c07d4d

" Comment grey
highlight Comment ctermfg=245 cterm=italic gui=italic guifg=#8a8a8a

" Change default vertical split separator color
highlight VertSplit ctermbg=8 guibg=grey cterm=bold

hi IncSearch ctermbg=246  ctermfg=black guibg=#3f4595 guifg=NONE
hi Search ctermbg=236  ctermfg=NONE guibg=#2a404e guifg=NONE

" Highlight search keywords 
" :noh to diable once or custom <ESC> followed by any action
set hlsearch

" Crosshair to find cursor :set cursorcolumn cursorline
hi CursorLine cterm=NONE ctermbg=35 ctermfg=NONE guibg=#1c2a34
hi CursorColumn cterm=NONE ctermbg=25 ctermfg=NONE

" Highlight default link myTodo Todo
if has("autocmd")
  " Highlight TODO, FIXME, NOTE, etc.
  if v:version > 701
    autocmd Syntax * call matchadd('Todo',  '\W\zs\(TODO\|FIXME\|CHANGED\|XXX\|BUG\|HACK\|OPTIMIZE\)')
    autocmd Syntax * call matchadd('Debug', '\W\zs\(NOTE\|INFO\|IDEA\)')
  endif
endif
highlight Debug ctermfg=yellow guifg=yellow

" highlight tabs and spaces and trailing spaces
" TODO toggle shortcut
set list
set lcs=tab:├─,nbsp:%
hi TabSymbol ctermfg=59 guifg=#19262f
au CursorHold * silent match TabSymbol /^\t\+/
