" In order to check vim default shortcuts :help index
" To view all user assigned shortcuts use `:map` OR  `:verbose map` for more
" details.
"
" To check a specific combination: `:map {{shortcut}}`

" Vim-Plug automatic installation
if has('nvim')
	let autoload_plug_path = stdpath('data') . '/site/autoload/plug.vim'
else
	let autoload_plug_path = '~/.vim/autoload/plug.vim'
endif
if empty(glob(autoload_plug_path))
  silent execute '!curl -fLo ' . autoload_plug_path . ' --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
unlet autoload_plug_path

" Specify a directory for plugins
" - For Vim: '~/.vim/plugged'
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
"call plug#begin('~/.vim/plugged')
"call plug#begin(stdpath('data') . '/plugged')
if has('nvim')
	call plug#begin(stdpath('data') . '/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

if has('nvim')
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
else
endif

if !exists('g:vscode')
	Plug 'scrooloose/nerdtree'
	Plug 'xuyuanp/nerdtree-git-plugin'

	Plug 'ctrlpvim/ctrlp.vim'

	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'joshdick/onedark.vim'
	Plug 'airblade/vim-gitgutter'
	Plug 'junegunn/goyo.vim'
	Plug 'junegunn/limelight.vim'
	Plug 'morhetz/gruvbox'
	Plug 'ryanoasis/vim-devicons'
	
	Plug 'unblevable/quick-scope'


	Plug 'pangloss/vim-javascript'
	Plug 'ap/vim-css-color'
	Plug 'posva/vim-vue'
	Plug 'MaxMEllon/vim-jsx-pretty'

	
	" NOTE tested on vscode, apparently not working
	Plug 'liuchengxu/vim-which-key'
	Plug 'mattn/emmet-vim'
	Plug 'easymotion/vim-easymotion'
elseif exists('g:vscode')
	" NOTE: This is fork of vim-easymotion to use with vscode-neovim extension
	" Doen't replace characters and cause errors
	Plug 'asvetliakov/vim-easymotion'
endif

Plug 'tpope/vim-fugitive'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'godlygeek/tabular'
Plug 'jiangmiao/auto-pairs'
Plug 'preservim/nerdcommenter'

" NOTE tested on vscode, apparently works nice
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'editorconfig/editorconfig-vim'

" NOTE: For C++ programming
"Plug 'bfrg/vim-cpp-modern'
"Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }
"Plug 'tikhomirov/vim-glsl' "For OpenGL GLSL shading language
"Plug 'jackguo380/vim-lsp-cxx-highlight'

" NOTE: Quite a bit slow and was unable to properly set custom highlighting as variables were not defined on startup
"Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" NOTE: Initialize plugin system
call plug#end()

let mapleader=","

set timeoutlen=800

" If hidden is not set, TextEdit might fail. And allow switching between
" buffers w/ saving
set hidden

" Write file history to a file
set undofile

" Display something in front of every brocken line (too long)
set showbreak=.\ 

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300


" NOTE: grammalecte TODO install before use
"let g:grammalecte_cli_py='~/Downloads/Apps/Grammalecte/grammalecte-cli.py'

"if has('nvim')
	"" Mouse scroll, taken from https://neovim.io/doc/user/scroll.html
	"map <ScrollWheelUp> <C-Y>
	"map <S-ScrollWheelUp> <C-U>
	"map <ScrollWheelDown> <C-E>
	"map <S-ScrollWheelDown> <C-D>
"endif

" Vim slows down when using this plugin: fix
" https://github.com/posva/vim-vue#vim-slows-down-when-using-this-plugin-how-can-i-fix-that
let g:vue_pre_processors = ['less', 'scss']
" CHANGED may not work properly, revert back to the previous one in case it
" doesn't work
"let g:vue_pre_processors = 'detect_on_enter'

" Move lines up or down with alt-j or alt-k
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv


" Enable 24-bit True Color
if !has('nvim') && has('termguicolors')
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

set termguicolors

" Better display for messages
set cmdheight=2

" don't give |ins-completion-menu| messages.
"set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Enable NeoVim mouse support in all modes
set mouse=a

set wrap
set linebreak

" Tell what is the syntax highlighting group used at the cursor
" https://vim.fandom.com/wiki/Identify_the_syntax_highlighting_group_used_at_the_cursor
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

if !exists('g:vscode')
	" NOTE: Keep the :highlight stuff below otherwise it gets cleared by the pluggin onedark
	colorscheme onedark
endif

" Doxygen
let g:load_doxygen_syntax=1
" JSDoc
let g:javascript_plugin_jsdoc = 1

set number relativenumber

" Automatically reload file if changed outside of Vim
set autoread

" Move cursor to matchin pattern
set incsearch
" Diasble search highlight
nnoremap && :nohlsearch<CR>

" Split on the right and below instead of left/top
set splitright
set splitbelow


"nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
set cursorline

set cindent
set smartindent
set smarttab
set autoindent

" Indent with Tabs having the size of two blank spaces
"set expandtab
set softtabstop=3
set tabstop=3
"set preserveindent "NOTE: messes things up, I can't remember why I ever set
"this on
" Use tabstop when auto indenting
" shiftwidth when set to 0 match tabstop
set shiftwidth=3


" Indent HTML files correctly see :h html-indent
let g:html_indent_script1 = "inc" 
let g:html_indent_style1 = "inc" 

" https://github.com/jackguo380/vim-lsp-cxx-highlight#configuration
" NOTE: goes with the plugin above
"let g:lsp_cxx_hl_use_text_props = 1


" Tabs move shortcuts
nnoremap tn :tabnew<Space>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

nnoremap <C-space> i<space><Esc>l
nnoremap <BS> X

nnoremap <expr> n (v:searchforward ? 'n' : 'N')
nnoremap <expr> N (v:searchforward ? 'N' : 'n')

nnoremap <expr> ; (getcharsearch().forward ? ';' : ',')
nnoremap <expr> , (getcharsearch().forward ? ',' : ';')

" Keep the cursor in position when scrolling until it reaches window's border
nnoremap <expr> <C-U> Scroll(line("w$"), line("w0"), 1)
nnoremap <expr> <C-D> Scroll(line("w$"), line("w0"), -1)
function! Scroll(endLine, startLine, direction)
  let l:numberOfLines=(a:endLine-a:startLine)/2 - 3
  if (a:direction == -1)
    return l:numberOfLines."\<C-e>"
  elseif (a:direction == 1) 
    return l:numberOfLines."\<C-y>"
  endif
endfunc

" Text format options see `:help fo-table`
set formatoptions=crqj
" c : Auto-wrap comments using textwidth, inserting the current comment leader automatically.
" r : Automatically insert the current comment leader after hitting <Enter> in Insert mode.
" q : Allow formatting of comments with "gq". Note that formatting will not change blank lines or lines containing only the comment leader.  A new paragraph starts after such a line, or when the comment leader changes.
" j : Where it makes sense, remove a comment leader when joining lines.  For
"  example, joining:
"  	int i;   // the index ~
"  	         // in the list ~
"  Becomes:
"  	int i;   // the index in the list ~

" Insterting lines while staying in normal mode
nmap oo o<Esc>k
nmap OO O<Esc>j

" Disable swap files
set noswapfile
"
" Look for ctags file and go up the folder structure until it's found
set tags=./tags;/

" Make `CTRL-A` and `CTRL-X` increase 1 by 1 in front of a number like 01
set nrformats-=octal

augroup matchingChar
  autocmd! matchingChar
  autocmd Filetype java,javascript,json,cpp,c,glsl,vue,typescript inoremap <buffer> ;; <End>;
  autocmd Filetype javascript,json,vue,typescript inoremap <buffer> ,, <End>,
augroup end

" NOTE facilitate move on line using quick scope
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

" Enter in insert mode makes the cursor get to a newline
" IDEA
inoremap <M-CR> <Esc>o

autocmd BufRead *.shader set filetype=glsl

" Open file in visual editor
" IDEA
" https://vimways.org/2018/opening-non-vim-file-formats/
"silent execute "!mplayer " . shellescape(expand("%:p")) . " &>/dev/null &" | buffer# | bdelete# | redraw! | syntax on
nmap <silent> gO :execute "!gedit ".shellescape(expand( "%:p" )) . " &>/dev/null &"<CR>

function! Open_with(program)
	if executable(a:program)
		silent execute "!" . a:program . " ".shellescape(expand( "%:p" )) . " &>/dev/null &"
	else
		echo "No program ".a:program." found on this machine."
	endif
endfunction

"NOTE: highlight yanked text
"from: https://www.youtube.com/watch?v=LLfeDkm-JUc
if exists('##TextYankPost')
	autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank('Substitute', 200)
endif

"NOTE: Move '"' register content into '+' (clipboard)
nnoremap <leader>s :silent let @+=@"<CR>:echo "Copied unamed register into clipboard"<CR>

" Load my highlight config files
let $VIMUSERCONFIG = '$HOME/.config/nvim/'
source $VIMUSERCONFIG/color/general.vim
source $VIMUSERCONFIG/color/javascript.vim
source $VIMUSERCONFIG/color/doxygen.vim

for f in split(glob('~/.config/nvim/plugin-config/*.vim'), "\n")
	execute 'source' f
endfor

if has('nvim')
	for f in split(glob('~/.config/nvim/plugin-config/nvim/*.vim'), "\n")
		execute 'source' f
	endfor
endif
