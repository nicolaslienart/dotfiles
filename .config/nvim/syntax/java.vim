" https://superuser.com/a/1386593/1029143
" Java: 'new', 'instanceof'
"highlight Operator ctermfg=5  guifg=#d175bc
" Java: 'this', 'super'
"highlight Typedef ctermfg=5  guifg=#d175bc
" Java: 'void', 'int', 'double'
"highlight Type ctermfg=4  guifg=#69b7d3


" Some more highlights, in addition to those suggested by cmcginty
highlight link javaScopeDecl Statement
highlight link javaType Type
highlight link javaDocTags PreProc

syntax match ClassName display '\<\([A-Z][a-z0-9]*\)\+\>'
syntax match ClassName display '\.\@<=\*'
highlight link ClassName Identifier

highlight javaBraces ctermfg=grey
