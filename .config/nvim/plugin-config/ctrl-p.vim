" INFO CTRL P config file
" --------------------------------
if !exists('g:vscode')
	" TODO check if it works
	let g:ctrlp_custom_ignore = {
				\ 'dir':  '\v[\/](\.(git)|node_modules|build|tmp)$',
				\ }
	let g:ctrlp_show_hidden = 1
endif
