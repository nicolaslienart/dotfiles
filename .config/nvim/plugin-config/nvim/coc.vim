" INFO CoC config file
" --------------------------------


if !exists('g:vscode')
	" COC extensions
	let g:coc_global_extensions = [ 'coc-word', 'coc-sh', 'coc-snippets', 'coc-prettier', 'coc-eslint', 'coc-vetur', 'coc-tsserver', 'coc-markdownlint', 'coc-emoji' ]

	" Use tab for trigger completion with characters ahead and navigate.
	" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
	inoremap <silent><expr> <TAB>
				\ pumvisible() ? "\<C-n>" :
				\ <SID>check_back_space() ? "\<TAB>" :
				\ coc#refresh()
	inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

	function! s:check_back_space() abort
		let col = col('.') - 1
		return !col || getline('.')[col - 1]  =~# '\s'
	endfunction

	" Use <c-space> to trigger completion.
	" INFO: already used by CtrlP plugin
	"inoremap <silent><expr> <c-space> coc#refresh()

	" Use `[g` and `]g` to navigate diagnostics
	nmap <silent> [g <Plug>(coc-diagnostic-prev)
	nmap <silent> ]g <Plug>(coc-diagnostic-next)

	" NOTE: Open definition under cursor...
	" ...In current window
	nmap <silent> gd <Plug>(coc-definition)
	" ...To a new tab
	nmap <silent> gD <c-w>v<c-w>T<Plug>(coc-definition)
	" ...To a new vertical split
	nmap <silent> gvd <c-w>v<Plug>(coc-definition)
	" ...To a new horizontal split
	nmap <silent> gsd <c-w>s<Plug>(coc-definition)

	nmap <silent> gdd <Plug>(coc-declaration)
	nmap <silent> gy <Plug>(coc-type-definition)
	"nmap <silent> gi <Plug>(coc-implementation)
	nmap <silent> gr <Plug>(coc-references)

	" Use K to show documentation in preview window
	" CHECK - OK
	nnoremap <silent> K :call <SID>show_documentation()<CR>

	function! s:show_documentation()
		if (index(['vim','help'], &filetype) >= 0)
			execute 'h '.expand('<cword>')
		else
			call CocAction('doHover')
		endif
	endfunction


	" Prettier specific format command
	command! -nargs=0 Prettier :CocCommand prettier.formatFile

	" Remap for rename current word
	nmap <leader>rn <Plug>(coc-rename)

	" Remap for format selected region
	vmap <leader>f  <Plug>(coc-format-selected)
	nmap <leader>f  <Plug>(coc-format-selected)

	augroup mygroup
		autocmd!
		" Setup formatexpr specified filetype(s).
		autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
		" TODO not quite clear
		" Update signature help on jump placeholder
		autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
	augroup end


	" NOTE not quite clear what it does
	" TODO check Coc documentation for codeaction
	" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
	"xmap <leader>a  <Plug>(coc-codeaction-selected)
	"nmap <leader>a  <Plug>(coc-codeaction-selected)

	" Remap for do codeAction of current line
	"nmap <leader>ac  <Plug>(coc-codeaction)


	" Fix autofix problem of current line
	" CHECK - WTF, when can I trigger this?
	nmap <leader>qf  <Plug>(coc-fix-current)

	" NOTE not quite clear what it does
	" TODO check Coc documentation for funobj
	" Create mappings for function text object, requires document symbols feature of languageserver.
	"xmap if <Plug>(coc-funcobj-i)
	"xmap af <Plug>(coc-funcobj-a)
	"omap if <Plug>(coc-funcobj-i)
	"omap af <Plug>(coc-funcobj-a)

	" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
	" TODO Check if ALT-D is not conflicting with something else
	" IDEA
	nmap <silent> <M-d> <Plug>(coc-range-select)
	xmap <silent> <M-d> <Plug>(coc-range-select)

	" Use `:Format` to format current buffer
	command! -nargs=0 Format :call CocAction('format')

	" TODO check how is this different from internal Vim folding
	" Use `:Fold` to fold current buffer
	"command! -nargs=? Fold :call     CocAction('fold', <f-args>)

	" TODO try it and see how it behave
	" use `:OR` for organize import of current buffer
	"command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

	" Add status line support, for integration with other plugin, checkout `:h coc-status`
	set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

	" Using CocList
	" Show all diagnostics
	nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
	" Manage extensions
	nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
	" Show commands
	nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
	" Find symbol of current document
	" TODO check what exactly are outlines
	nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
	" Search workspace symbols
	nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
	" Do default action for next item.
	nnoremap <silent> <space>j  :<C-u>CocNext<CR>
	" Do default action for previous item.
	nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
	" Resume latest coc list
	nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

	" Map sass to scss for language servers
	let g:coc_filetype_map = {
				\ 'sass': 'scss'
				\ }

	" Highlight symbol under cursor on CursorHold
	autocmd CursorHold * silent call CocActionAsync('highlight')
	au CursorHoldI * sil call CocActionAsync('showSignatureHelp')

	highlight CocHighlightText ctermbg=NONE ctermfg=4 guifg=#61afef cterm=underline,italic gui=underline,italic 

	" Snippets ------
	" Use <C-l> for trigger snippet expand.
	imap <C-l> <Plug>(coc-snippets-expand)

	" Use <C-j> for select text for visual placeholder of snippet.
	vmap <C-j> <Plug>(coc-snippets-select)

	" Use <C-j> for jump to next placeholder, it's default of coc.nvim
	let g:coc_snippet_next = '<c-j>'

	" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
	let g:coc_snippet_prev = '<c-k>'

	" Use <C-j> for both expand and jump (make expand higher priority.)
	imap <C-j> <Plug>(coc-snippets-expand-jump)

	" Use <leader>x for convert visual selected code to snippet
	xmap <leader>x  <Plug>(coc-convert-snippet)

	inoremap <silent><expr> <TAB>
				\ pumvisible() ? coc#_select_confirm() :
				\ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
				\ <SID>check_back_space() ? "\<TAB>" :
				\ coc#refresh()

	function! s:check_back_space() abort
		let col = col('.') - 1
		return !col || getline('.')[col - 1]  =~# '\s'
	endfunction

	let g:coc_snippet_next = '<tab>'
endif
