" INFO vim-airline config file
" --------------------------------

let g:airline_powerline_fonts = 1
" PLUGIN vim-airline enable tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
