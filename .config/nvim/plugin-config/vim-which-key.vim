" INFO vim-which-key config file
" --------------------------------

autocmd VimEnter * call which_key#register(',', "g:which_key_map")
nnoremap <silent> <leader> :WhichKey ','<CR>

" Define prefix dictionary
let g:which_key_map = {}

" Second level dictionaries:
" 'name' is a special field. It will define the name of the group, e.g.,
" leader-h is the "+gitgutter group"
" Unamed groups will show a default empty string.

" ========================================================
" Create menius based on existing mappings
" ========================================================
" You can pass a descriptive text to an existing mapping.

let g:which_key_map.h = { 'name' : '+gitgutter' }
let g:which_key_map.e = { 'name' : '+NERDTree' }
let g:which_key_map.c = { 'name' : '+NERDCommenter' }
