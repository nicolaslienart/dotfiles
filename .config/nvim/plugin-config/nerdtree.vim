" INFO NERDTree config file
" --------------------------------
if !exists('g:vscode')
	nnoremap <Leader>e :NERDTreeToggle<CR>
	nnoremap <Leader>E :NERDTreeFind<CR>

	" NERDTree filter out .class files for Java*
	let NERDTreeIgnore=['\.class$', '\~$']

	" https://github.com/scrooloose/nerdtree/blob/master/README.markdown#how-can-i-open-nerdtree-automatically-when-vim-starts-up-on-opening-a-directory
	autocmd StdinReadPre * let s:std_in=1
	autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

	" https://github.com/scrooloose/nerdtree/blob/master/README.markdown#how-can-i-close-vim-if-the-only-window-left-open-is-a-nerdtree
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


	" NERDTree syntax highlight full name when using a match
	"let g:NERDTreeExactMatchHighlightFullName = 1
	"let g:NERDTreePatternMatchHighlightFullName = 1

	"let g:NERDTreeExtensionHighlightColor['vue'] = "FF0000" " set vue files color to green
endif
