" INFO Netrw config file
" --------------------------------

" Netrw file browser tweaks
let g:netrw_winsize = 25
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_banner = 0

let g:netrw_bufsettings="modifiable nomodified number relativenumber nowrap readonly buflisted"

autocmd FileType netrw setl bufhidden=wipe

function! ToggleNetrw()
  let found = 0
  for i in range(1, bufnr("$"))
    if buflisted(i)
      if getbufvar(i, '&filetype') == "netrw"
        let found = 1
        silent execute 'bwipeout ' . i
      endif
    endif
  endfor
  if !found
    Vexplore
  endif
endfunction

