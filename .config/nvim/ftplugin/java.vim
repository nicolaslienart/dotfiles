" java ftplugin file
" Filetypes:	*.java

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
 finish
endif

" Don't load another plugin for this buffer
let b:did_ftplugin = 1


let java_highlight_functions = 1
let java_highlight_all = 1
