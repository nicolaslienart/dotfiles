# don't put duplicate lines or lines starting with space in the history.
# # See bash(1) for more options
HISTCONTROL=ignoreboth

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

## Set editor to NeoVim
export EDITOR='/usr/local/bin/nvim'
export VISUAL=$EDITOR
export SUDO_EDITOR=$EDITOR
shopt cdable_vars

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac


# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


## Directory shortcuts
## use cd <saved_shortcut>
## cloc_save to save a new shortcut pointing the current directory (PWD)
alias cloc_show='cat ~/.saved_dirs'

function cloc_save() {
  	here=$(pwd)
  	if [ $# -eq 0 ]
  	then
    	name=$(basename $here)
  	elif [ $# -eq 1 ]
  	then
    	name=$1
  	elif [ $# -eq 2 ]
  	then
    	name=$(basename $2)
  	elif [ $# > 2 ]
  	then
    	echo "usage: save [<name>] OR cloc_save [<name>] [<location>]"
    	return -1
  	fi

  	sed -i -e "/^$name=/d" ~/.saved_dirs
  	echo "$name=\"$here\"" >> ~/.saved_dirs
  	source ~/.saved_dirs
}

source ~/.saved_dirs

export PATH=~/.local/bin:$PATH

## Open NeoVim with `e`
e() {
  	nvim $1
}

## https://superuser.com/a/1397388/1029143
## Special ranger function to gradually move directory 
function ranger-cd() {
  	# create a temp file and store the name
  	tempfile="$(mktemp -t tmp.XXXXXX)"

    # run ranger and ask it to output the last path into the
    # temp file
    ranger --choosedir="$tempfile" "${@:-$(pwd)}"

    # if the temp file exists read and the content of the temp
    # file was not equal to the current path
    test -f "$tempfile" &&
       if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        	 # change directory to the path in the temp file
        	 cd -- "$(cat "$tempfile")"
       fi

    # its not super necessary to have this line for deleting
    # the temp file since Linux should handle it on the next
    # boot
    rm -f -- "$tempfile"
 }
## Use above function with the shortcut `nav`
alias nav="ranger-cd"

## Long listing alias
alias ll="ls -l --all --human-readable"

## Open TMUX on terminal session startup
if [ "$TMUX" = "" ]; then tmux; fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
