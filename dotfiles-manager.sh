#!/bin/bash

shopt nocasematch

# Determine location of this script file
CURRENT_PATH="`dirname \"$0\"`"
if [ "$1" = "backup" ]
then
	# rsync to merge into dest directory
	mkdir -p $CURRENT_PATH/.vim/mySnips $CURRENT_PATH/.config/nvim

	rsync --archive --update ~/.vim/mySnips/ $CURRENT_PATH/.vim/mySnips
	rsync --archive --update ~/.config/nvim/ $CURRENT_PATH/.config/nvim
	# --update : only if source is more recent
	#cp --update ~/.vimrc $CURRENT_PATH/
	cp --update ~/.tmux.conf $CURRENT_PATH/
	cp --update ~/.zshrc $CURRENT_PATH/
	cp --update ~/.bashrc $CURRENT_PATH/
	cp --update ~/.p10k.zsh $CURRENT_PATH/
	cp --update ~/.Xresources $CURRENT_PATH/
	cp --update ~/.gitconfig $CURRENT_PATH/
	cp --update ~/.config/fontconfig/fonts.conf $CURRENT_PATH/.config/fontconfig/
	rsync --archive --update ~/.config/fontconfig/conf.d/ $CURRENT_PATH/.config/fontconfig/conf.d

	echo 'Updating the git repository'
	read -p 'Commit (y)es/(n)o? ' BACKUP_FILES
	if [[ $BACKUP_FILES =~ (yes|y)$ ]]
	then
		git -C $CURRENT_PATH add .
		#read -p "Commit message: " COMMIT_MESSAGE
		#git -C $CURRENT_PATH commit --message="$COMMIT_MESSAGE"
		git -C $CURRENT_PATH commit
		git -C $CURRENT_PATH push
	else
		echo "Make sure you commit your backup"
	fi

elif [ "$1" = "deploy" ]
then
	if [ "$2" = "env" ]
	then
		echo "Deploying environment"
		echo "Adding missing utility packages"
		sudo apt install xsel wget rsync git curl python3 python python3-pip nodejs npm python-pip
		echo "Adding productivity packages"
		sudo apt install tmux zsh ranger

		read -p 'Change current user shell to ZSH (y)es/(n)o? ' INSTALL_ZSH
		if [[ $INSTALL_ZSH =~ (yes|y)$ ]]
		then
			echo "Updating shell path to ZSH"
			sudo usermod -s /usr/bin/zsh $(whoami)
		fi
		if [ ! -x '/usr/local/bin/nvim' ]
		then
			read -p 'Neovim not found, would you like to install it (y)es/(n)o? ' INSTALL_NVIM
			if [[ $INSTALL_NVIM =~ (yes|y)$ ]]
			then
				#NOTE: Install neovim nightly
				curl --fail --location --output $CURRENT_PATH/nvim-linux64.tar.gz https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.tar.gz
				tar -xf $CURRENT_PATH/nvim-linux64.tar.gz
				sudo cp -r nvim-linux64/share /usr/local/
				sudo cp -r nvim-linux64/man /usr/local/
				sudo cp -r nvim-linux64/bin /usr/local/
				sudo pip3 install neovim
				sudo pip2 install neovim
				sudo npm install -g neovim
			fi
		fi

		#NOTE: Download Melso NERDFont 
		mkdir -p ~/.fonts

		wget --content-disposition --no-verbose --show-progress -P ~/.fonts 'https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Meslo/M-DZ/Bold/complete/Meslo%20LG%20M%20DZ%20Bold%20Nerd%20Font%20Complete%20Mono.ttf?raw=true'
		wget --content-disposition --no-verbose --show-progress -P ~/.fonts 'https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Meslo/M-DZ/Italic/complete/Meslo%20LG%20M%20DZ%20Italic%20Nerd%20Font%20Complete%20Mono.ttf?raw=true'
		wget --content-disposition --no-verbose --show-progress -P ~/.fonts 'https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Meslo/M-DZ/Regular/complete/Meslo%20LG%20M%20DZ%20Regular%20Nerd%20Font%20Complete%20Mono.ttf?raw=true'
		wget --content-disposition --no-verbose --show-progress -P ~/.fonts 'https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/Meslo/M-DZ/Bold-Italic/complete/Meslo%20LG%20M%20DZ%20Bold%20Italic%20Nerd%20Font%20Complete%20Mono.ttf?raw=true'
	fi

	read -p "Would you like to backup existing files (y)es/(n)o? " BACKUP_FILES
	if [[ $BACKUP_FILES =~ (yes|y)$ ]]
	then
		echo "Creating backup file"
		mv --interactive ~/.vimrc ~/.vimrc.BACKUP
		[ $? -eq 0 ] && echo "Found .vimrc"
		mv --interactive ~/.config/nvim/init.vim ~/.config/nvim/init.vim.BACKUP
		[ $? -eq 0 ] && echo "Found init.vim for NeoVim"
		mv --interactive ~/.tmux.conf ~/.tmux.conf.BACKUP
		[ $? -eq 0 ] && echo "Found .tmux.conf"
		mv --interactive ~/.zshrc ~/.zshrc.BACKUP
		[ $? -eq 0 ] && echo "Found .zshrc"
		mv --interactive ~/.p10k.zsh ~/.p10k.zsh.BACKUP
		[ $? -eq 0 ] && echo "Found .p10k.zsh"
		mv --interactive ~/.vim/mySnips/ ~/.vim/mySnips.BACKUP/
		[ $? -eq 0 ] && echo "Found .vim/mySnips"
		mv --interactive ~/.Xresources ~/.Xresources.BACKUP/
		[ $? -eq 0 ] && echo "Found .Xresources"
		mv --interactive ~/.gitconfig ~/.gitconfig.BACKUP/
		[ $? -eq 0 ] && echo "Found .gitconfig"
		mv --interactive ~/.bashrc ~/.bashrc.BACKUP/
		[ $? -eq 0 ] && echo "Found .bashrc"
		mv --interactive ~/.config/fontconfig/fonts.conf ~/.config/fontconfig/fonts.conf.BACKUP/
		[ $? -eq 0 ] && echo "Found .config/fontconfig/fonts.conf"
		mv --interactive ~/.config/fontconfig/conf.d ~/.config/fontconfig/conf.d.BACKUP/
		[ $? -eq 0 ] && echo "Found .config/fontconfig/conf.d"
		echo "Backup complete"
	fi

	touch ~/.saved_dirs
	#echo "Copying .vimrc"
	#cp $CURRENT_PATH/.vimrc ~/
	echo "Copying .zshrc"
	cp $CURRENT_PATH/.zshrc ~/
	echo "Copying .gitconfig"
	cp $CURRENT_PATH/.gitconfig ~/
	echo "Copying .bashrc"
	cp $CURRENT_PATH/.bashrc ~/
	echo "Copying .p10k.zsh"
	cp $CURRENT_PATH/.p10k.zsh ~/
	echo "Copying .tmux.conf"
	cp $CURRENT_PATH/.tmux.conf ~/
	echo "Copying .Xresources"
	cp $CURRENT_PATH/.Xresources ~/
	echo "Copying ~/.vim/mySnips"
	mkdir -p ~/.vim/mySnips
	rsync --archive $CURRENT_PATH/.vim/mySnips ~/.vim/


	echo "Copying ~/.config/nvim/"
	mkdir -p ~/.config/nvim/mySnips
	rsync --archive $CURRENT_PATH/.config/nvim ~/.config/
	read -p "Would you like to symlink init.vim to .vimrc (y)es/(n)o? " SYM_VIM
	if [[ $SYM_VIM =~ (yes|y)$ ]]
	then
		ln --symbolic --force ~/.config/nvim/init.vim ~/.vimrc
	fi
	# Install Coc and Coc related stuff like nodejs, moved nodejs into apt install, setup is with Plug and coc-extensions within neovim
	#/bin/bash coc-setup.sh

	echo "Copying ~/.config/fontconfig/fonts.conf"
	mkdir -p ~/.config/fontconfig/
	cp $CURRENT_PATH/.config/fontconfig/fonts.conf ~/.config/fontconfig/fonts.conf
	rsync --archive $CURRENT_PATH/.config/fontconfig/conf.d ~/.config/fontconfig/
	fc-cache -f

	tmux source ~/.tmux.conf
	if [[ $SHELL == "/usr/bin/zsh" ]]
	then
		source ~/.zshrc
	fi
	xrdb ~/.Xresources

elif [ "$1" = "revert" ]
then
	echo "TODO: revert changes replace BACKUP files and maybe remove unecessary stuff"
elif [ "$1" = "get" ]
then
	echo "TODO: CLONE git repo just using this shell script"
	sudo apt install git
	git clone https://framagit.org/nicolaslienart/dotfiles.git
	cd dotfiles
	./dotfiles-manager.sh deploy
	# Executed through the following command
	#sh -c "$(wget https://framagit.org/nicolaslienart/dotfiles/raw/master/dotfiles-manager.sh -O -) get"
else
	echo "TODO: ELSE case"
fi
