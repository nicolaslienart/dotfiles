" Vundle plugin
"  git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ

Plugin 'ycm-core/YouCompleteMe'
Plugin 'SirVer/ultisnips'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tikhomirov/vim-glsl'
Plugin 'tpope/vim-surround'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required


" ᳁ CATEGORY: Testing
" ⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷

inoremap <M-<Leader>>" :echo "lol"
" ⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶



" ᳁ CATEGORY: Finding available shortcuts
" ⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷
" In order to check vim default shortcuts :help index
" To view all user assigned shortcuts use `:map` OR  `:verbose map` for more
" details.
"
" To check a specific combination: `:map {{shortcut}}`
" ⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶


" ᳁ CATEGORY: Visual options
" ⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷
let mapleader=","

set number relativenumber

" Automatically reload file if changed outside of Vim
set autoread

" Highlight search keywords 
" :noh to diable once or custom <ESC> followed by any action
set hlsearch
set incsearch
nnoremap && :nohlsearch<CR>

" Split on the right and below instead of left/top
set splitright
set splitbelow

" Indent with bank spaces instead of tabulations
set expandtab
set shiftwidth=2
set softtabstop=2

" Netrw file browser tweaks
"let g:netrw_winsize = 25
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4

let g:netrw_bufsettings="modifiable nomodified number relativenumber nowrap readonly buflisted"

autocmd FileType netrw setl bufhidden=wipe

function! ToggleNetrw()
  let found = 0
  for i in range(1, bufnr("$"))
    if buflisted(i)
      if getbufvar(i, '&filetype') == "netrw"
        let found = 1
        silent execute 'bwipeout ' . i
      endif
    endif
  endfor
  if !found
    Vexplore
  endif
endfunction

nnoremap <Leader>e :call ToggleNetrw()<CR>

" Crosshair to find cursor :set cursorcolumn cursorline
highlight CursorLine cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
highlight CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>

set cindent
set smartindent
"set autoindent

" Highlight comments like OPTIMIZE, TODO, FIXME, NOTE, IDEA
"syntax match myTodo /\v<(TODO|FIXME|NOTE|OPTIMIZE)\:*/ containedin=.*Comment
"highlight default link myTodo Todo

if has("autocmd")
  " Highlight TODO, FIXME, NOTE, etc.
  if v:version > 701
    autocmd Syntax * call matchadd('Todo',  '\W\zs\(TODO\|FIXME\|CHANGED\|XXX\|BUG\|HACK\|OPTIMIZE\)')
    autocmd Syntax * call matchadd('Debug', '\W\zs\(NOTE\|INFO\|IDEA\)')
  endif
endif

autocmd BufReadPost,BufNewFile .vimrc call matchadd('Separator', '\v^" ((⏶+)|(⏷+))') | call matchadd('SeparatorTitle', '\v^" ᳁.+')
highlight Separator cterm=bold ctermfg=DarkMagenta guifg=DarkMagenta
highlight SeparatorTitle cterm=bold,underline ctermbg=DarkMagenta ctermfg=white

" ⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶


" ᳁ CATEGORY: Moving options
" ⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷

" Fixing Control + arrow
noremap <ESC>[1;5C <C-Right>
noremap <ESC>[1;5D <C-Left>
noremap! <ESC>[1;5C <C-Right>
noremap! <ESC>[1;5D <C-Left>

" Tabs move shortcuts
nnoremap tn :tabnew<Space>
nmap <S-J>  :tabprev<CR>
nmap <S-K> :tabnext<CR>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

" Tabs relative relocattion shortcuts
"nmap <C-S-J>  :-tabmove<cr>
"nmap <C-S-K> :+tabmove<cr>
"nnoremap tj :tabprev<CR>
"nnoremap tk :tabnext<CR>

nnoremap <expr> n (v:searchforward ? 'n' : 'N')
nnoremap <expr> N (v:searchforward ? 'N' : 'n')

nnoremap <expr> ; (getcharsearch().forward ? ';' : ',')
nnoremap <expr> , (getcharsearch().forward ? ',' : ';')

" Keep the cursor in position when scrolling until it reaches window's border
nnoremap <expr> <C-U> Scroll(line("w$"), line("w0"), 1)
nnoremap <expr> <C-D> Scroll(line("w$"), line("w0"), -1)
function! Scroll(endLine, startLine, direction)
  let l:numberOfLines=(a:endLine-a:startLine)/2 - 3
  if (a:direction == -1)
    return l:numberOfLines."\<C-e>"
  elseif (a:direction == 1) 
    return l:numberOfLines."\<C-y>"
  endif
endfunc

" ⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶


" Insterting lines and stay in normal mode
"map <Enter> o<ESC>

" Text format options see `:help fo-table`
set formatoptions=crq
" c : Auto-wrap comments using textwidth, inserting the current comment leader automatically.
" r : Automatically insert the current comment leader after hitting <Enter> in Insert mode.
" q : Allow formatting of comments with "gq". Note that formatting will not change blank lines or lines containing only the comment leader.  A new paragraph starts after such a line, or when the comment leader changes.

" Insterting lines while staying in normal mode
nmap oo o<Esc>k
nmap OO O<Esc>j

" Disable swap files
set noswapfile
"
" Look for ctags file and go up the folder structure until it's found
set tags=./tags;/

" Make `CTRL-A` and `CTRL-X` increase 1 by 1 in front of a number like 01
set nrformats-=octal

" Pathogen plugin manager
" execute pathogen#infect()
" syntax on
" filetype plugin indent on

" Automatically add closing tag
" Based on: https://stackoverflow.com/questions/21316727/automatic-closing-brackets-for-vim#34992101
augroup matchingChar
  autocmd! matchingChar
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> "" ""<left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> '' ''<left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer>  `` ``<left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> () ()<left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> (<Space>) (<Space><Space>)<left><left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> [] []<left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> {} {<Space><Space>}<left><left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> {<Space>} {<Space><Space>}<left><left>
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> {<CR>} {<CR>}<ESC>O
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> ;; <End>;
  autocmd Filetype javascript,json,cpp,c,glsl inoremap <buffer> ,, <End>,<CR>
  autocmd Filetype javascript,json inoremap <buffer> .. <End>.


  autocmd Filetype zsh inoremap <buffer> [<Space>] [<Space><Space>]<left><left> 
  autocmd Filetype zsh inoremap <buffer> {<CR>} {<CR>}<ESC>O
  autocmd Filetype zsh inoremap <buffer> if<Space>fi if<Space>fi<left><left> 
  autocmd Filetype zsh inoremap <buffer> "" ""<left>
  autocmd Filetype zsh inoremap <buffer> '' ''<left>
  autocmd Filetype zsh inoremap <buffer> () ()<left>
augroup end


" Enter in insert mode makes the cursor get to a newline
inoremap <CR> <Esc>o
inoremap <S-CR> <Esc>o

"iunmap <C-J>
"unmap i_CTRL-J

let g:airline_powerline_fonts = 1
" PLUGIN vim-airline enable tabline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" VIM UltiSnip https://github.com/SirVer/ultisnips
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/mySnips/', 'UltiSnips']


" https://stackoverflow.com/questions/14896327/ultisnips-and-youcompleteme#answer-25533430
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]

" https://vi.stackexchange.com/questions/4056/is-there-an-easy-way-to-close-a-scratch-buffer-preview-window
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1

let g:ycm_global_ycm_extra_conf='~/.vim/bundle/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'

" YCM Completer FixIt trigger
nnoremap <Leader>f :YcmCompleter FixIt<CR>


" ᳁ CATEGORY: Old stuff
" ⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷⏷
" Netrw window management
"let g:lastNetrwBufferId = 0
"" Netrw last buffer close
"function! LastBufSel(pattern)
"  let curBuffer = bufnr("$") -1
"  while curBuffer && g:lastNetrwBufferId == 0
"    if(bufexists(curBuffer))
"      let curBufferName = bufname(curBuffer)
"      if(match(curBufferName, a:pattern) > -1 && buflisted(curBuffer))
"        let g:lastNetrwBufferId = curBuffer
"      endif
"    endif
"    let curBuffer -= 1
"  endwhile
"  if (!curBuffer)
"    echo "Netrw not open"
"  endif
"endfunction
"
"nnoremap <Leader>q :call LastBufSel("Netrw*")<CR>:bdelete! <c-r>=g:lastNetrwBufferId<CR><CR>:let g:lastNetrwBufferId=0<CR>
"nnoremap <Leader>e :Vexplore<CR>
"nnoremap <Leader>q <C-w>hZQ<C-w>l

" ⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶⏶
